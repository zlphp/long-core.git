<?php
declare(strict_types=1);

namespace LongCore;

use LongCore\Traits\ControllerTrait;

/**
 * API接口控制器基类
 */
abstract class LongApi
{
    use ControllerTrait;
}
