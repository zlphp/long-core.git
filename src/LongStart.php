<?php

declare(strict_types=1);

namespace LongCore;

use Hyperf\Framework\Bootstrap\ServerStartCallback;
use LongCore\Interfaces\ServiceInterface\ModuleServiceInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class LongStart extends ServerStartCallback
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function beforeStart()
    {
        $service = container()->get(ModuleServiceInterface::class);
        $service->setModuleCache();
        $console = console();
        $console->info('Long-admin start success...');
        str_contains(PHP_OS, 'CYGWIN') && $console->info('current booting the user: ' . shell_exec('whoami'));
    }
}