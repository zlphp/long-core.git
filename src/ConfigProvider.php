<?php

declare(strict_types=1);

namespace LongCore;

use LongCore\Annotation\DependProxyCollector;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                    'collectors' => [
                        DependProxyCollector::class,
                    ]
                ],
            ],
            'commands' => [],
            'listeners' => [],
            'publish' => [
                [
                    'id' => 'admin_config',
                    'description' => 'long-admin config file.',
                    'source' => __DIR__ . '/../publish/longadmin.php',
                    'destination' => BASE_PATH . '/config/autoload/longadmin.php',
                ],
                [
                    'id' => 'translatable_config',
                    'description' => 'The config for translatable.',
                    'source' => __DIR__ . '/../publish/translatable.php',
                    'destination' => BASE_PATH . '/config/autoload/translatable.php',
                ],
            ],
        ];
    }
}
