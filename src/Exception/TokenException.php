<?php
declare(strict_types=1);

namespace LongCore\Exception;

class TokenException extends LongException
{
}