<?php

declare(strict_types=1);

namespace LongCore\Crontab\Mutex;

use LongCore\Crontab\LongCrontab;

interface ServerMutex
{
    /**
     * Attempt to obtain a server mutex for the given crontab.
     * @param LongCrontab $crontab
     * @return bool
     */
    public function attempt(LongCrontab $crontab): bool;

    /**
     * Get the server mutex for the given crontab.
     * @param LongCrontab $crontab
     * @return string
     */
    public function get(LongCrontab $crontab): string;
}
