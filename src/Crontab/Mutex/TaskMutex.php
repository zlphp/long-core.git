<?php
declare(strict_types=1);

namespace LongCore\Crontab\Mutex;

use LongCore\Crontab\LongCrontab;

interface TaskMutex
{
    /**
     * Attempt to obtain a task mutex for the given crontab.
     * @param LongCrontab $crontab
     * @return bool
     */
    public function create(LongCrontab $crontab): bool;

    /**
     * Determine if a task mutex exists for the given crontab.
     * @param LongCrontab $crontab
     * @return bool
     */
    public function exists(LongCrontab $crontab): bool;

    /**
     * Clear the task mutex for the given crontab.
     * @param LongCrontab $crontab
     */
    public function remove(LongCrontab $crontab);
}
