<?php
declare(strict_types=1);

namespace LongCore\Command\Migrate;

use Hyperf\Database\Migrations\MigrationCreator;

class LongMigrationCreator extends MigrationCreator
{

    public function stubPath(): string
    {
        return BASE_PATH . '/vendor/zhaolong/long-core/src/Command/Migrate/Stubs';
    }
}
