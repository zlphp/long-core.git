<?php

declare(strict_types=1);

namespace LongCore\Generator;

interface CodeGenerator
{
    public function generator();

    public function preview();
}