<?php
declare(strict_types=1);

namespace LongCore;

use Hyperf\Di\Annotation\Inject;
use LongCore\Traits\ControllerTrait;

/**
 * 后台控制器基类
 */
abstract class LongController
{
    use ControllerTrait;

    /**
     * @var Long
     */
    #[Inject]
    protected Long $long;
}
