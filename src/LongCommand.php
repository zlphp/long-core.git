<?php
declare(strict_types=1);

namespace LongCore;

use Hyperf\Command\Command as HyperfCommand;

/**
 * class LongCommand
 * @package System
 */
abstract class LongCommand extends HyperfCommand
{
    protected string $module;

    protected const CONSOLE_GREEN_BEGIN = "\033[32;5;1m";
    protected const CONSOLE_RED_BEGIN = "\033[31;5;1m";
    protected const CONSOLE_END = "\033[0m";

    protected function getGreenText($text): string
    {
        return self::CONSOLE_GREEN_BEGIN . $text . self::CONSOLE_END;
    }

    protected function getRedText($text): string
    {
        return self::CONSOLE_RED_BEGIN . $text . self::CONSOLE_END;
    }

    protected function getStub($filename): string
    {
        return BASE_PATH . '/vendor/zhaolong/long-core/src/Command/Creater/Stubs/' . $filename . '.stub';
    }

    protected function getModulePath(): string
    {
        return BASE_PATH . '/app/' . $this->module . '/Request/';
    }

    protected function getInfo(): string
    {
        return '';
    }
}
